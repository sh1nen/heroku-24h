package com.stosik.client;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class ElenxClientFirst {

  public static void main(String[] args) {

    new SpringApplicationBuilder(ElenxClientFirst.class)
        .bannerMode(Banner.Mode.OFF)
        .headless(true)
        .application()
        .run(args);
  }
}