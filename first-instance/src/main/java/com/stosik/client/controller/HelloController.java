package com.stosik.client.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/")
public class HelloController {


  private final Environment env;

  @GetMapping("/hello")
  public String helloWorld() {

    return "Hello world from client on port: " + env.getProperty("local.server.port");
  }
}