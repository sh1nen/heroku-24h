package com.stosik.proxy.configuration;

import com.stosik.proxy.configuration.rules.HighAvailabilityRule;
import com.netflix.loadbalancer.IRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HighAvailabilityConfiguration {

  @Bean
  public IRule highAvailabilityRule() {
    return new HighAvailabilityRule();
  }
}
