package com.stosik.proxy.configuration.rules;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AbstractLoadBalancerRule;
import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.Server;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@Slf4j
public class HighAvailabilityRule extends AbstractLoadBalancerRule {

  public HighAvailabilityRule(ILoadBalancer lb) {
    this();
    this.setLoadBalancer(lb);
  }

  public Server choose(Object key) {
    return this.choose(this.getLoadBalancer(), key);
  }

  private Server choose(ILoadBalancer lb, Object o) {

    if (lb == null) {
      log.warn("no load balancer");
      return null;
    } else {
      Server server = null;
      int count = 0;

      while (true) {
        if (count++ < 10) {
          List<Server> reachableServers = lb.getReachableServers();
          List<Server> allServers = lb.getAllServers();
          int upCount = reachableServers.size();
          int serverCount = allServers.size();
          if (upCount != 0 && serverCount != 0) {
            int nextServerIndex = retrieveCurrentHour() % reachableServers.size();
            server = allServers.get(nextServerIndex);
            if (server == null) {
              Thread.yield();
            } else {
              if (server.isAlive() && server.isReadyToServe()) {
                return server;
              }
            }
            continue;
          }
          log.warn("No up servers available from load balancer: " + lb);
          return null;
        }

        if (count >= 10) {
          log.warn("No available alive servers after 10 tries from load balancer: " + lb);
        }
      }
    }
  }

  private int retrieveCurrentHour() {
    return LocalDateTime.now().getHour();
  }

  @Override
  public void initWithNiwsConfig(IClientConfig iClientConfig) {

  }
}
