package com.stosik.proxy.configuration;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Configuration;

@Configuration
@RibbonClient(name = "first-instance", configuration = HighAvailabilityConfiguration.class)
public class ZuulProxyConfiguration {
}
