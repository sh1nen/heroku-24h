package com.stosik.proxy;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableDiscoveryClient
@EnableZuulProxy
@SpringBootApplication
public class ZuulProxy {

  public static void main(String[] args) {

    new SpringApplicationBuilder(ZuulProxy.class)
        .bannerMode(Banner.Mode.OFF)
        .headless(true)
        .application()
        .run(args);
  }
}
