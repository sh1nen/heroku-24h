package com.stosik.eureka.server;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class EurekaServer {

  public static void main(String[] args) {

    new SpringApplicationBuilder(EurekaServer.class)
        .bannerMode(Banner.Mode.OFF)
        .headless(true)
        .application()
        .run(args);
  }
}
