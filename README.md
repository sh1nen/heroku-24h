# Heroku-24h

Free heroku app available for 24/7 :)

### A little bit about services

first-instance - instances run on random ports, they use eureka to register themselves with standalone eureka-server run locally

eureka-server - app which works as the server waiting for intances to register themselves

zuul-proxy - application which also register itself with eureka-server, it also has dependency from netlifx-zuul-oss
which allows to route, load-balance and filter requests all at once.

### Problem

Free instance of heroku application can not be available for 24 hours a day and respond to requests as we wish. Each instance on heroku
has assigned dynos (think of them as containers used to run your application on), each free account has assigned "dyno hours" which you can not
cross (1000 dyno hours montly for verified and 550 for unverifired account). My goal was to deploy 4 instances of the same application which
receives GET requests and connect them in one unit through proxy which routes the requests to each instance depending on the current hour throughout
the day as follows:

*(0, 4, ..., 20) - 1st instance

*(1, 5,..., 21) - 2nd instance

*(2, 6, ..., 22) - 3rd instance

*(3, 7, ..., 23) - 4th instance

### How did I manage to do that ?
Keeping in mind KISS rule, I had one goal "do not make it harder than it is". Then I went for a cup of coffee :D
After I run four instances of the same app and eureka server which holds their ip addresses I run another one responsible for routing requests to the specific
server based on the custom load balancing stratagy aka HighAvailabilityRule(yes, I came up with it on my own). Unfortunately, when it comes to Zuul's load balancing strategies
he has none of his own, but uses implementations of Ribbon (another fancy lib from Netflix) for client load balancing vodoo. So the only thing I had to do, was to define my own
@Configuration bean, and override default IRule implementation which is RoundRoubinRule. Not going further into details I implemented choosing intance of server based on following 
algorithm(hash function):

```java
int nextServerIndex = retrieveCurrentHour() % reachableServers.size();
```
where simply the result of modulo of the current hour (let's say it's 8pm.) and number of reachable servers( I have four of them) gives me:

```
nextServerIntex = 7 % 4
nextServerIndex = 3 (4th instance)
```
